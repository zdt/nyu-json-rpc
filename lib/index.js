"use strict"

const Peer = require("json-rpc-peer").default

const protocol = require("json-rpc-protocol")
const MethodNotFound = protocol.MethodNotFound
const InvalidParameters = protocol.InvalidParameters

function _flattenParams(params) {
	if (params.length === 1) {
		const first = params[0]

		if (typeof first === "object" && first !== null) {
			const proto = Object.getPrototypeOf(first)
			if (proto === Object.prototype || proto === null) {
				return first
			}
		}
	}

	return params
}


function RpcDispatcher(send) {
	this.peer = new Peer(this._dispatch.bind(this))

	this.peer.on("data", function (chunk) {
		if (Buffer.isBuffer(chunk)) chunk = chunbk.toString("utf8")
		if (!chunk.substr) chunk = JSON.stringify(chunk)
		try {
			send(chunk)
		} catch (err) {
			this.peer.failPendingRequests(err)
		}
	}.bind(this))

	this.endPoints = {}
}

RpcDispatcher.prototype.request = function request(method) {
	const params = Array.prototype.slice.call(arguments, 1)
	return this.peer.request(method, _flattenParams(params))
}

RpcDispatcher.prototype.notify = function notify(method) {
	const params = Array.prototype.slice.call(arguments, 1)
	return this.peer.notify(method, _flattenParams(params))
}

RpcDispatcher.prototype.execute = function execute(message) {
	return this.peer.exec(message).then(function (result) {
		if (result !== undefined) this.peer.push(result)
	}.bind(this))
}

RpcDispatcher.prototype.registerFunction = function registerFunction(fn, name) {
	const ep = name? name: fn.name
	if (!ep) throw new Error("make sure to give your function an alias!")
	if (this.endPoints[ep]) throw new Error("method <" + ep + "> already registered")

	this.endPoints[ep] = fn
}

RpcDispatcher.prototype.registerObject = function registerObject(o, ns) {
	const prefix = ns? ns + ".": ""

	Object.getOwnPropertyNames(o)
		.filter(function (name) { return typeof o[name] === "function" })
		.forEach(function (name) { return this.registerFunction(o[name].bind(o), prefix + name) }.bind(this))
}

RpcDispatcher.prototype._dispatch = function _dispatch(message) {
	// does the method exist?
	if (!this.endPoints[message.method]) throw new MethodNotFound()

	// try calling the method
	try {
		// build parameter array
		const params = []
		if (Array.isArray(message.params)) params.push.apply(null, message.params)
		else params.push(message.params)

		// call endpoint
		return this.endPoints[message.method].apply(null, params)
	} catch (err) {
		// throw errors as InvalidParameters
		if (err instanceof InvalidParameters) throw err
		throw new InvalidParameters(err.name + ": " + err.message)
	}
}

module.exports = RpcDispatcher
